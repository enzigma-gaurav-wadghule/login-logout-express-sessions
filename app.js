var express = require('express')
var parseurl = require('parseurl')
var session = require('express-session')

var FileStore = require('session-file-store')(session)
var app = express()

var MongoDBStore = require('connect-mongodb-session')(session);
 
var app = express();
var store = new MongoDBStore({
  uri: 'mongodb://localhost:27017/connect_mongodb_session_test',
  collection: 'mySessions'
});
 
// Catch errors
store.on('error', function(error) {
  console.log(error);
});

// Use the session middleware
app.use(session({ secret: 'keyboard cat', 
cookie: { maxAge: 60000 },
resave:false,
saveUninitialized: true,
store: store
}))

app.use(function (req, res, next) {
  if (!req.session.views) {
    req.session.views = {}
  }

  // get the url pathname
  var pathname = parseurl(req).pathname

  // count the views
  req.session.views[pathname] = (req.session.views[pathname] || 0) + 1
  req.session.views['Ip']= req.connection.remoteAddress
  next()
})

app.get('/login', function (req, res, next) {
  res.send('you viewed this page ' + req.session.views['/login'] + ' times')
})

app.get('/logout',(req,res,next)=>{
    req.session.destroy(function(err) {
        res.clearCookie("connect.sid");
       res.send('you are logged out successfully!!')
      })
})

app.listen(3000,()=>{
    console.log("server is listening on 3000");
});